from django.shortcuts import render, redirect
from .models import Item
from redis import Redis
import mongoengine

redis = Redis(host='redis', port=6379)


def home(request):
    if request.method == 'POST':
        Item.objects.create(text=request.POST['item_text'])
        return redirect('/')
    items = Item.objects.all()
    counter = redis.incr('counter')
    return render(request, 'home.html', {'items': items, 'counter': counter})


def scan(request):
	MONGODB_HOST = os.environ.get('DB2_PORT_27017_TCP_ADDR', '127.0.0.1')
	mongoengine.connect(host=MONGODB_HOST)
    items = Item.objects.all()
    return render(request, 'scan.html', {'items': items})    
